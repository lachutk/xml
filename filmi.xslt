<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="utf-8"/>
	<xsl:template match="/">

		<html>
			<head>
				<meta charset="utf-8"/>
				<title>Filmi</title>
				<link rel="stylesheet" href="style.css" />
			</head>

			<body>
				<h1>Filmi</h1>
				<table>
					<tr>
						<th>Nazwa:</th>
						<th>Gatunek(główny):</th>
						<th>Gatunek(poboczny):</th>
						<th>Reżyser:</th>
						<th>Premiera(Polska)</th>
						<th>Premiera(Świat)</th>
						<th>Okładka</th>
					</tr>
					<xsl:for-each select="//Film">
						<tr>
							<xsl:choose>
								<xsl:when test="@type='film'">
								
									<td bgcolor="pink"><xsl:value-of select="nazwa"/></td>
									<td bgcolor="pink"><xsl:value-of select="gatunki/gatunek[@poziom='glowny']"/></td>
									<td bgcolor="pink"><xsl:value-of select="gatunki/gatunek[@poziom='poboczny']"/></td>
									<td bgcolor="pink"><xsl:value-of select="rezyser"/></td>
									<td bgcolor="pink"><xsl:value-of select="premiery/premiera[@gdzie='polska']"/></td>
									<td bgcolor="pink"><xsl:value-of select="premiery/premiera[@gdzie='swiat']"/></td>
									<td bgcolor="pink"><img><xsl:attribute name="src">
									<xsl:value-of select="okladka"/>
									</xsl:attribute></img></td>
								
								</xsl:when>

								<xsl:otherwise>
								
									<td bgcolor="gray"><xsl:value-of select="nazwa"/></td>
									<td bgcolor="gray"><xsl:value-of select="gatunki/gatunek[@poziom='glowny']"/></td>
									<td bgcolor="gray"><xsl:value-of select="gatunki/gatunek[@poziom='poboczny']"/></td>
									<td bgcolor="gray"><xsl:value-of select="rezyser"/></td>
									<td bgcolor="gray"><xsl:value-of select="premiery/premiera[@gdzie='polska']"/></td>
									<td bgcolor="gray"><xsl:value-of select="premiery/premiera[@gdzie='swiat']"/></td>
									<td bgcolor="gray"><img><xsl:attribute name="src">
									<xsl:value-of select="okladka"/>
									</xsl:attribute></img></td>
								
								</xsl:otherwise>

							</xsl:choose>
						</tr>
					</xsl:for-each>
				</table>
				
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>